import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.*;
import AlphabetSoupSolution.AlphabetSoup;

public class AlphabetSoupTest {

    @Test
    public void test1() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/default_case_1.txt", "ABC 0:0 0:2\nAEI 0:0 2:2\n");
    }

    @Test
    public void test2() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/default_case_2.txt", "HELLO 0:0 4:4\nGOOD 4:0 4:3\nBYE 1:3 1:1\n");
    }

    @Test
    public void test3() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/single_letter.txt", "A 0:0 0:0\n");
    }
    
    @Test
    public void test4() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/no_match.txt", "");
    }
    
    @Test
    public void test5() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/horizontal_cases.txt", "ABCD 0:0 0:3\nEFGH 1:0 1:3\nIJKL 2:0 2:3\nMNOP 3:0 3:3\nDCBA 0:3 0:0\nHGFE 1:3 1:0\nLKJI 2:3 2:0\nPONM 3:3 3:0\n");
    }
    
    @Test
    public void test6() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/word_too_long.txt", "");
    }

    @Test
    public void test7() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/large_input.txt", "DAMAGING 1:1 8:1\nGOOOOOOOOD 9:0 9:9\n");
    }
    
    @Test
    public void test8() throws FileNotFoundException
    {
        testCase("src/test/java/testcases/nonsquare.txt", "ABC 0:0 0:2\n");
    }
    
    public void testCase(String filePath, String expectedOutput) throws FileNotFoundException 
    {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        String[] input = {filePath};
        AlphabetSoup.main(input);
        assertEquals(expectedOutput, outContent.toString());
    }
}