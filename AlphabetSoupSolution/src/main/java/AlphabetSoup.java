package AlphabetSoupSolution;

import java.io.*;
import java.util.*;

public class AlphabetSoup
{
    private static int[] horizontalShift = {-1, -1, -1, 0, 0, 1, 1, 1};
    private static int[] verticalShift = {-1, 0, 1,-1, 1, 0, 1,-1};

    public static void main( String[] args) throws FileNotFoundException
    {
        Scanner scanner = new Scanner(new File(args[0]));

        String[] dimensions = scanner.nextLine().split("x");
        int rowSize = Integer.parseInt(dimensions[0]);
        int colSize = Integer.parseInt(dimensions[1]);

        char[][] grid = new char[rowSize][colSize];
        
        for (int r = 0; r < rowSize; r++)
        {
            String[] line = scanner.nextLine().split(" ");
            for (int c = 0; c < colSize; c++)
            {
                grid[r][c] = line[c].charAt(0);
            }
        }
        
        while (scanner.hasNext()) {
            String currWord = scanner.nextLine();
            boolean found = false;

            for (int r = 0; r < rowSize; r++)   // loop through the grid
            {
                for (int c = 0; c < colSize; c++)   // loop through the grid
                {
                    for (int dir = 0; dir < 8; dir++)   // check for the current word in all eight directions
                    {
                        
                        if (wordSearch(r, c, currWord, grid, dir))
                        {
                            System.out.println(currWord + " " + r + ":" + c + " " + (r + (currWord.length() - 1) * horizontalShift[dir]) + ":" + (c + (currWord.length() - 1) * verticalShift[dir]));
                            found = true;
                        }
                        if (found)
                        {
                            break;
                        }
                    }
                    if (found)
                    {
                        break;
                    }
                }
                if (found)
                {
                    break;
                }
            }
        }
        scanner.close();
    }

    private static boolean wordSearch(int row, int col, String word, char[][] grid, int dir)
    {
        int length = word.length();
        int prevRow = row + (length - 1) * horizontalShift[dir];
        int prevCol = col + (length - 1) * verticalShift[dir];

        if (prevRow < 0 || prevRow >= grid.length || prevCol < 0 || prevCol >= grid[0].length)  // if direction goes out of range, impossible to find word
        {
            return false;
        }

        for (int i = 0; i < length; i++)  // continuously search in the direction until the end of the word is reached, or wrong letter
        {
            if (grid[row][col] != word.charAt(i)) {
                return false;
            }

            row += horizontalShift[dir];
            col += verticalShift[dir];
        }

        return true;

    }
}
